package com.paytm.qim.utility;

import com.paytm.qim.data.excel.DataReaderUtil;

public class CommonUtility {

	public static String cutStartString(String searchString, String startTag){
		
		String extractedValue;
		extractedValue =searchString.substring(searchString.indexOf(startTag)+startTag.length());
		return extractedValue;
	}
	
	public static int getRowNum(String uri){
		int rowNum=0;
		for(int i=1; i<15; i++){
			String API_URI=DataReaderUtil.readFromExcel(i, 1, 0);
			System.out.println("API Data at "+i+" row is "+API_URI);
			if(API_URI.equals(uri)){
				rowNum=i;
				System.out.println("rowNum is "+rowNum);
				break;
			}
		}
		return rowNum;
	}
	
	
}

