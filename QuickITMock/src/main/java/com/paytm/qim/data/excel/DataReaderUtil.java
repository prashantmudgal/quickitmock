package com.paytm.qim.data.excel;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class DataReaderUtil {

	private static final String file_Path="/home/prashantmudgal/Performance Testng/QIM/Data/APIData.xls";
	private static XSSFSheet ExcelWSheet;
	private static XSSFWorkbook ExcelWBook;
	private static XSSFCell Cell;

	public static final String readFromExcel(int RowNum, int ColNum, int sheetnumber ) {

		try{
			FileInputStream ExcelFile = new FileInputStream(file_Path);
			ExcelWBook = new XSSFWorkbook(ExcelFile);
			ExcelWSheet = ExcelWBook.getSheetAt(sheetnumber);
			Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);
			String CellData = Cell.getStringCellValue();
			return CellData;
		}catch (Exception e){
			return "";
		}
	}

	public final static String readFromText(){

		try {
			File file = new File("/home/prashantmudgal/Performance Testng/QIM/Data/test.txt");

			BufferedReader br = new BufferedReader(new FileReader(file));
			String st;
			st = br.readLine();
		    return st;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}
}

