package com.paytm.qim.controller.helloWorld;

import javax.servlet.http.HttpServletRequest;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.paytm.qim.data.excel.DataReaderUtil;
import com.paytm.qim.utility.CommonUtility;

@Controller
@RequestMapping({"*","*/*","*/*/*","*/*/*/*"})
@Scope("request")
public class HelloWorld {

	//final String URL1="/helloworld";
	//final String URL2=URL1;
	//String URL1=DataReaderUtil.ReadFromExcel(1, 1, 0);
	//final static String test=DataReaderUtil.readFromText();
	//System.out.print(test+"*****************"+URL1+"########"+URL2);
	//System.out.println("");
	//final static String bc= "" + test;
	//int rowNum;

	String responseBody=null;
	

	@RequestMapping
	public String URIHandling(HttpServletRequest request){
		String requestURL=request.getRequestURL().toString();
		String requestURI=CommonUtility.cutStartString(requestURL, "/QuickITMock");
		//System.out.println("requested URI is "+requestURI);
		int row=CommonUtility.getRowNum(requestURI);
		String requestHeader=DataReaderUtil.readFromExcel(row, 5, 0);
		return requestHeader;
	}
	
	
	@ResponseBody
	public String defaultHandling(HttpServletRequest request){

		String responseBody=null;
		String requestURL=request.getRequestURL().toString();
		String requestURI=CommonUtility.cutStartString(requestURL, "/QuickITMock");
		System.out.println("requested URI is "+requestURI);
		
		int rowNum=0;
		for(int i=1; i<8; i++){
			String API_URI=DataReaderUtil.readFromExcel(i, 1, 0);
			System.out.println("API Data at "+i+" row is "+API_URI);
			if(API_URI.equals(requestURI)){
				rowNum=i;
				System.out.println("rowNum is "+rowNum);
				break;
			}
		}
		
		
		responseBody=DataReaderUtil.readFromExcel(rowNum, 7, 0);
		System.out.println("response body is "+responseBody);
		
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return responseBody;
	}
	
	
	@RequestMapping("/healthCheck/*")
	@ResponseBody
	public String healthAPIHandling(HttpServletRequest request){

		String responseBody=null;
		
		
		responseBody="hello "+requestHeader;
		
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return responseBody;
	}
	
}